macro(bs_solctra_setup_options)
  option(bs_solctra_TREAT_WARNINGS_AS_ERRORS "Treat warnings as errors" ON)
  option(bs_solctra_KNL_ARCHITECTURE "Compile for KNL architecture" OFF)

endmacro(bs_solctra_setup_options)

macro(bs_solctra_local_options)
  include(CheckCXXCompilerFlag)
  include(${CMAKE_SOURCE_DIR}/cmake/StandardProjectSettings.cmake)

  add_library(bs_solctra_warnings INTERFACE)
  add_library(bs_solctra_options INTERFACE)

  include(cmake/CompilerWarnings.cmake)
  bs_solctra_set_project_warnings(bs_solctra_warnings ${bs_solctra_TREAT_WARNINGS_AS_ERRORS} "")

  if(bs_solctra_TREAT_WARNINGS_AS_ERRORS)
    check_cxx_compiler_flag("-Wl,--fatal-warnings" LINKER_FATAL_WARNINGS)
  endif()

  if(bs_solctra_KNL_ARCHITECTURE)
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -ffast-math -march=knl -mavx512f -mavx512pf -mavx512er -mavx512cd")
    set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO} -ffast-math -march=knl -mavx512f -mavx512pf -mavx512er -mavx512cd")

    else()
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -ffast-math -march=native")
  endif()

endmacro(bs_solctra_local_options)
