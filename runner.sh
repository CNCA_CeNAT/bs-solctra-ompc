ranks=(2 4 8 12)
steps=(2000)
ratios=(1)
particle_numbers=(1024 2048 4096 8192)

base_dir=`pwd`
for rank in ${ranks[@]}; do
    for step_num in ${steps[@]}; do
        for ratio in ${ratios[@]}; do
           for particle_number in ${particle_numbers[@]}; do
               new_dir=run_$rank/$step_num\_steps_r$ratio/$particle_number
               new_file=$rank\_$particle_number.slurm
               mkdir -p $new_dir
               cp ./runtemplate.slurm $new_dir/$new_file
               cd $new_dir
               sed -i "s/%nodes%/$rank/" ./$new_file
               sed -i "s/%prefix%/$rank\_$particle_number/" ./$new_file
               sbatch --array=0-9 ./$new_file $step_num $ratio $particle_number
               cd $base_dir
            done
        done
    done
done
