# BS-SOLCTRA (OpenMP Cluster implementation)

This repository holds an implementation of [BS-SOLCTRA](https://link.springer.com/chapter/10.1007/978-3-030-41005-6_3) using the [OpenMP Cluster](https://ompcluster.gitlab.io/) parallel programming framework.

To work with this project it is recommended that you use the Docker images provided by the OpenMP Cluster team (see for example the `.devcontainer/Dockerfile` file).  
In the `configurations/` directory there's an example of running the software locally (`run.sh`) or in a SLURM cluster (`run_base.slurm`).
