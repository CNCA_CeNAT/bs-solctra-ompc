#ifndef SOLCTRA_SOLCTRA_H
#define SOLCTRA_SOLCTRA_H

#include <cstddef>
#include <string_view>
#include <utils.h>

#pragma omp declare target
void runParticles(const Coils &coils, Coils &e_roof, const LengthSegments &length_segments,
                  const std::string_view output, Particle *particles,
                  const size_t total_particles, const size_t start_position, const unsigned int rank,
                  const unsigned int steps, const double &step_size,
                  const unsigned int mode);
#pragma omp end declare target
#endif
