#include <algorithm>
#include <cmath>
#include <cstddef>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <fstream>
#include <iostream>
#include <iterator>
#include <omp.h>
#include <ostream>
#include <solctra_multinode.h>
#include <sstream>
#include <string>
#include <utils.h>
#include <vector>

constexpr auto DEFAULT_STEPS = 100000u;
constexpr auto DEFAULT_STEP_SIZE = 0.001;
constexpr auto DEFAULT_MODE = 1u;
constexpr auto DEFAULT_RATIO= 1;
const auto DEFAULT_RESOURCES = std::string("resources");
constexpr auto DEFAULT_MAGPROF = 0u;
constexpr auto DEFAULT_NUM_POINTS = 10000u;
constexpr auto DEFAULT_PHI_ANGLE = 0;
constexpr auto DEFAULT_DIMENSION = 1u;
constexpr auto DEFAULT_DEBUG = 0u;

auto getStepsFromArgs(const int &argc, char **argv) {
  for (int i = 1; i < argc - 1; ++i) {
    std::string tmp = argv[i];
    if (tmp == "-steps") {
      return static_cast<unsigned>(atoi(argv[i + 1]));
    }
  }
  return DEFAULT_STEPS;
}

auto getStepSizeFromArgs(const int &argc, char **argv) {
  for (int i = 1; i < argc - 1; ++i) {
    std::string tmp = argv[i];
    if (tmp == "-stepSize") {
      return strtod(argv[i + 1], nullptr);
    }
  }
  return DEFAULT_STEP_SIZE;
}

void LoadParticles(const int &argc, char **argv, Particles &particles,
                   const unsigned int length, const int seedValue) {
  bool found = false;
  for (int i = 1; i < argc - 1; ++i) {
    std::string tmp = argv[i];
    if (tmp == "-particles") {
      loadParticleFile(particles, length, argv[i + 1]);
      found = true;
      break;
    }
  }
  if (!found) {
    std::cout << "No file given. Initializing random particles\n";
    initializeParticles(particles, seedValue);
  }
}

auto getResourcePath(const int &argc, char **argv) {
  for (int i = 1; i < argc - 1; ++i) {
    std::string param = argv[i];
    if ("-resource" == param) {
      return std::string(argv[i + 1]);
    }
  }
  return DEFAULT_RESOURCES;
}

auto getParticlesLengthFromArgs(const int &argc, char **argv) {
  for (int i = 1; i < argc - 1; ++i) {
    std::string tmp = argv[i];
    if (tmp == "-length") {
      return static_cast<unsigned>(atoi(argv[i + 1]));
    }
  }
  std::cerr << "ERROR: You must specify number of particles to simulate\n";
  exit(1);
}

unsigned getDebugFromArgs(const int &argc, char **argv) {
  for (int i = 1; i < argc - 1; ++i) {
    std::string tmp = argv[i];
    if (tmp == "-d") {
      return static_cast<unsigned>(atoi(argv[i + 1]));
    }
  }
  return DEFAULT_DEBUG;
}

unsigned getModeFromArgs(const int &argc, char **argv) {
  for (int i = 1; i < argc - 1; ++i) {
    std::string tmp = argv[i];
    if (tmp == "-mode") {
      return static_cast<unsigned>(atoi(argv[i + 1]));
    }
  }
  return DEFAULT_MODE;
}

unsigned getParallelizationRatio(const int &argc, char **argv) {
  for (int i = 1; i < argc - 1; ++i) {
    std::string tmp = argv[i];
    if (tmp == "-ratio") {
      return static_cast<unsigned>(atoi(argv[i + 1]));
    }
  }
  return DEFAULT_RATIO;
}

std::string getJobId(const int &argc, char **argv) {
  for (int i = 1; i < argc - 1; ++i) {
    std::string tmp = argv[i];
    if (tmp == "-id") {
      return std::string(argv[i + 1]);
    }
  }
  std::cerr << "ERROR: job id must be given!!\n";
  exit(1);
}

unsigned getMagneticProfileFromArgs(const int &argc, char **argv) {
  for (int i = 1; i < argc - 1; ++i) {
    std::string tmp = argv[i];
    if (tmp == "-magnetic_prof") {
      return static_cast<unsigned>(atoi(argv[i + 1]));
    }
  }
  return DEFAULT_MAGPROF;
}

unsigned getNumPointsFromArgs(const int &argc, char **argv) {
  for (int i = 1; i < argc - 1; ++i) {
    std::string tmp = argv[i];
    if (tmp == "-magnetic_prof") {
      return static_cast<unsigned>(atoi(argv[i + 2]));
    }
  }
  return DEFAULT_NUM_POINTS;
}

unsigned getAngleFromArgs(const int &argc, char **argv) {
  for (int i = 1; i < argc - 1; ++i) {
    std::string tmp = argv[i];
    if (tmp == "-magnetic_prof") {
      return static_cast<unsigned>(atoi(argv[i + 3]));
    }
  }
  return DEFAULT_PHI_ANGLE;
}

unsigned getDimension(const int &argc, char **argv) {
  for (int i = 1; i < argc - 1; ++i) {
    std::string tmp = argv[i];
    if (tmp == "-magnetic_prof") {
      return static_cast<unsigned>(atoi(argv[i + 4]));
    }
  }
  return DEFAULT_DIMENSION;
}

int main(int argc, char **argv) {
  const auto nworkers{omp_get_num_devices()};
  const auto resourcePath{getResourcePath(argc, argv)};
  const auto steps{getStepsFromArgs(argc, argv)};
  const auto stepSize{getStepSizeFromArgs(argc, argv)};
  const auto length{getParticlesLengthFromArgs(argc, argv)};
  const auto mode{getModeFromArgs(argc, argv)};
  const auto ratio{getParallelizationRatio(argc, argv)};
  const auto debugFlag{getDebugFromArgs(argc, argv)};
  const auto magprof{getMagneticProfileFromArgs(argc, argv)};
  const auto num_points{getNumPointsFromArgs(argc, argv)};
  const auto phi_angle{getAngleFromArgs(argc, argv)};
  const auto jobId{getJobId(argc, argv)};
  const auto dimension{getDimension(argc, argv)};
  const auto output{"results_" + jobId};
  createDirectoryIfNotExists(output);

  std::cout << "Number of workers=[" << nworkers << "]." << std::endl;
  std::cout << "Running with:" << std::endl;
  std::cout << "Resource Path=[" << resourcePath << "]." << std::endl;
  std::cout << "JobId=[" << jobId << "]." << std::endl;
  std::cout << "Steps=[" << steps << "]." << std::endl;
  std::cout << "Steps size=[" << stepSize << "]." << std::endl;
  std::cout << "Particles=[" << length << "]." << std::endl;
  std::cout << "Input Current=[" << I << "] A." << std::endl;
  std::cout << "Parallelization Ratio=[" << ratio << "]." << std::endl;
  std::cout << "Mode=[" << mode << "]." << std::endl;
  std::cout << "Output path=[" << output << "]." << std::endl;
  std::string file_name = "stdout_" + jobId + ".log";

  std::ofstream handler;
  handler.open(file_name.c_str());
  if (!handler.is_open()) {
    std::cerr << "Unable to open stdout.log for appending. Nothing to do."
              << std::endl;
    exit(0);
  }

  handler << "Running with:" << std::endl;
  handler << "Steps=[" << steps << "]." << std::endl;
  handler << "Steps size=[" << stepSize << "]." << std::endl;
  handler << "Particles=[" << length << "]." << std::endl;
  handler << "Mode=[" << mode << "]." << std::endl;
  handler << "Parallelization Ratio=[" << ratio << "]." << std::endl;
  handler << "Output path=[" << output << "]." << std::endl;
  handler << "Number of workers=[" << nworkers << "]." << std::endl;

  Particles particles(length);

  auto startInitializationTime = 0.0;

  if (debugFlag) {
    startInitializationTime = omp_get_wtime();
  }
  LoadParticles(argc, argv, particles, length, 0);
  if (debugFlag) {
    const auto endInitializationTime = omp_get_wtime();
    std::cout << "Total initialization time=["
              << (endInitializationTime - startInitializationTime) << "]."
              << std::endl;
  }

  std::cout << "Particles initialized\n";
  const auto nworkers_init{static_cast<size_t>(nworkers)};

  const auto total_shares = nworkers_init * ratio;
  std::vector<unsigned int> share_size(total_shares);
  const auto base_share = length / static_cast<unsigned int>(total_shares);
  for (size_t i = 0; i < total_shares; ++i) {
    share_size[i] = base_share;
    if (i < length % static_cast<unsigned int>(total_shares)) {
      share_size[i] += 1;
    }
  }
  std::cout << "Shared size initialized\n";

  std::vector<unsigned int> start_positions(total_shares);
  start_positions[0] = 0;

  for (size_t i = 1; i < total_shares; i++) {
    start_positions[i] = start_positions[i - 1] + share_size[i - 1];
  }
  std::cout << "Start positions initialized\n";

  Coils coils;
  loadCoilData(coils, resourcePath);
  std::cout << "Coil data loaded" << std::endl;

  Coils e_roof;
  LengthSegments length_segments;

  computeERoof(coils, e_roof, length_segments);
  std::cout << "e_roof data computed" << std::endl;

  if (magprof != 0) {
    computeMagneticProfile(coils, e_roof, length_segments, num_points,
                           phi_angle, dimension);
    std::cout << "magnetic profiles data computed" << std::endl;
  }

  const auto startTime = omp_get_wtime();

  std::cout << "Executing simulation" << std::endl;

  auto particles_ptr = particles.data();

  Timings timings(total_shares); 
  #pragma omp parallel
  #pragma omp single
  {
  auto output_c = output.data();
  #pragma omp target enter data map(to: output_c[:output.size()]) depend(out: output_c) nowait
  
  auto share_size_ptr = share_size.data();
  #pragma omp target enter data map(to: share_size_ptr[:share_size.size()]) depend(out: share_size_ptr) nowait

  auto start_positions_ptr = start_positions.data();
  #pragma omp target enter data map(to: start_positions_ptr[:start_positions.size()]) depend(out: start_positions_ptr) nowait
  #pragma omp target enter data map(to: particles_ptr[:particles.size()]) depend(out: particles_ptr) nowait

  for (unsigned int i = 0; i < total_shares; ++i) {
  const auto sp = static_cast<const int>(start_positions_ptr[i]);
  const auto sh = static_cast<const int>(share_size_ptr[i]);
    #pragma omp target nowait\
    depend(iterator(j = sp:sp + sh), in: particles_ptr[j])
    {
    runParticles(coils, e_roof, length_segments, output_c,
                particles_ptr, share_size_ptr[i], start_positions_ptr[i],
                i, steps, stepSize, mode);
    }
  }
  }

  for(const auto & timing: timings)
  {
    std::cout << timing << '\n';
  }
  const double endTime = omp_get_wtime();
  std::cout << "Simulation finished" << std::endl;
  std::cout << "Total execution time=[" << (endTime - startTime) << "]."
            << std::endl;
  handler << "Total execution time=[" << (endTime - startTime) << "]."
          << std::endl;
  handler.close();
  handler.open("stats.csv", std::ofstream::out | std::ofstream::app);
  if (!handler.is_open()) {
    std::cerr << "Unable to open stats.csv for appending. Nothing to do."
              << std::endl;
    exit(0);
  }
  handler << jobId << "," << length << "," << steps << "," << stepSize << ","
          << output << "," << (endTime - startTime) << std::endl;
  handler << "half1" << ' ' << "half2" << ' ' << "total" << '\n';
  for(const auto & timing: timings)
  {
      handler << timing << '\n';
  }
  handler.close();
  time_t now = time(0);
  char *dt = ctime(&now);

  std::cout << "Timestamp: " << dt << std::endl;
  return 0;
}
