#include <cassert>
#include <cmath>
#include <fstream>
#include <iostream>
#include <solctra_multinode.h>
#include <sstream>
#include <utils.h>
#include <omp.h>

void printIterationFileTxt(Particle *particles, const size_t total_particles,
                           const unsigned int iteration,
                           const unsigned int rank,
                           const size_t start_pos,
                           const std::string_view output) {
  constexpr auto max_double_digits = std::numeric_limits<double>::max_digits10;
  std::ostringstream filename_ss;
  filename_ss << output << "/iteration_" << iteration << "_" << rank << ".txt";
  std::ofstream handler(filename_ss.str());

  if (handler.bad()) {
    std::cerr << "Unable to open file=[" << filename_ss.str()
              << "]. Nothing to do\n";
    exit(-1);
  }
  handler << "x,y,z\n";
  handler.precision(max_double_digits);
  for (size_t i = start_pos; i < start_pos + total_particles; ++i) {
    handler << particles[i] << '\n';
  }
  handler.close();
}

void printRankExecutionTimeFile(const double compTime,
                                const std::string &output, const int rank_id) {

  std::ostringstream rankstring;
  rankstring << rank_id;
  std::string valueRank = rankstring.str();

  FILE *handler;
  std::string file_name = output + "/rank_" + valueRank + "_compTime.txt";
  handler = fopen(file_name.c_str(), "a");
  if (nullptr == handler) {
    std::cerr << "Unable to open file=[" << file_name << "]. Nothing to do\n";
    exit(0);
  }

  fprintf(handler, "%f,", compTime);

  fclose(handler);
}

void printExecutionTimeFile(const double compTime,
                            const std::string_view output, const int progress) {

  std::ostringstream filename_ss;
  filename_ss << output << "/exec_compTime.txt";
  auto file_name = filename_ss.str();
  auto handler = fopen(file_name.c_str(), "a");
  if (nullptr == handler) {
    std::cerr << "Unable to open file=[" << file_name << "]. Nothing to do\n";
    exit(0);
  }

  if (progress == 0) {
    fprintf(handler, "Halfway execution time: %f\n", compTime);
  }

  if (progress == 1) {
    fprintf(handler, "Second half execution time: %f\n", compTime);
  }

  if (progress == 2) {
    fprintf(handler, "Total execution time: %f\n", compTime);
  }
  fclose(handler);
}

bool computeIteration(const Coils &coils, const Coils &e_roof,
                      const LengthSegments &length_segments,
                      Particle &start_point, const double &step_size,
                      const unsigned int mode, int &divergenceCounter) {
  Particle p1;
  Particle p2;
  Particle p3;

  Cartesian k1;
  Cartesian k2;
  Cartesian k3;
  Cartesian k4;

  Cartesian zero_vect;
  Particle p;
  Cartesian r_vector;

  Coils rmi;
  Coils rmf;

  constexpr double half = 1.0 / 2.0;
  k1 = computeMagneticField(coils, e_roof, rmi, rmf, length_segments,
                            start_point);
  auto norm_temp = 1.0 / norm_of(k1);
  k1.x = (k1.x * norm_temp) * step_size;
  k1.y = (k1.y * norm_temp) * step_size;
  k1.z = (k1.z * norm_temp) * step_size;
  p1.x = (k1.x * half) + start_point.x;
  p1.y = (k1.y * half) + start_point.y;
  p1.z = (k1.z * half) + start_point.z;

  k2 = computeMagneticField(coils, e_roof, rmi, rmf, length_segments, p1);
  norm_temp = 1.0 / norm_of(k2);
  k2.x = (k2.x * norm_temp) * step_size;
  k2.y = (k2.y * norm_temp) * step_size;
  k2.z = (k2.z * norm_temp) * step_size;
  p2.x = (k2.x * half) + start_point.x;
  p2.y = (k2.y * half) + start_point.y;
  p2.z = (k2.z * half) + start_point.z;

  k3 = computeMagneticField(coils, e_roof, rmi, rmf, length_segments, p2);
  norm_temp = 1.0 / norm_of(k3);
  k3.x = (k3.x * norm_temp) * step_size;
  k3.y = (k3.y * norm_temp) * step_size;
  k3.z = (k3.z * norm_temp) * step_size;
  p3.x = k3.x + start_point.x;
  p3.y = k3.y + start_point.y;
  p3.z = k3.z + start_point.z;

  k4 = computeMagneticField(coils, e_roof, rmi, rmf, length_segments, p3);
  norm_temp = 1.0 / norm_of(k4);
  k4.x = (k4.x * norm_temp) * step_size;
  k4.y = (k4.y * norm_temp) * step_size;
  k4.z = (k4.z * norm_temp) * step_size;
  start_point.x = start_point.x + ((k1.x + 2 * k2.x + 2 * k3.x + k4.x) / 6);
  start_point.y = start_point.y + ((k1.y + 2 * k2.y + 2 * k3.y + k4.y) / 6);
  start_point.z = start_point.z + ((k1.z + 2 * k2.z + 2 * k3.z + k4.z) / 6);

  auto diverged = false;
  if (mode == 1) {
    p.x = start_point.x;
    p.y = start_point.y;
    zero_vect.x = (p.x / norm_of(p)) * MAJOR_RADIUS; //// Origen vector
    zero_vect.y = (p.y / norm_of(p)) * MAJOR_RADIUS;
    zero_vect.z = 0;
    r_vector.x = start_point.x - zero_vect.x;
    r_vector.y = start_point.y - zero_vect.y;
    r_vector.z = start_point.z - zero_vect.z;
    const auto r_radius = norm_of(r_vector);
    if (r_radius > MINOR_RADIUS) {
      start_point.x = MINOR_RADIUS;
      start_point.y = MINOR_RADIUS;
      start_point.z = MINOR_RADIUS;
      divergenceCounter += 1;
      diverged = true;
    }
  }
  return diverged;
}

void runParticles(const Coils &coils, Coils &e_roof, const LengthSegments &length_segments,
                  const std::string_view output, Particle *particles,
                  const size_t total_particles, const size_t start_position, const unsigned int rank,
                  const unsigned int steps, const double &step_size,
                  const unsigned int mode) {

  printIterationFileTxt(particles, total_particles, 0, rank, start_position, output);
  auto divergenceCounter = 0;
  for (unsigned int step = 1; step <= steps; ++step) {
// #pragma omp parallel for
    for (size_t i = start_position; i < start_position + total_particles; ++i) {
      if ((particles[i].x == MINOR_RADIUS) &&
          (particles[i].y == MINOR_RADIUS) &&
          (particles[i].z == MINOR_RADIUS)) {
        continue;
      } else {
        computeIteration(coils, e_roof, length_segments, particles[i],
                         step_size, mode, divergenceCounter);
      }
    }
    // if (step % 10 == 0) {
      // printIterationFileTxt(particles, total_particles, step, rank, start_position, output);
    // }
  }
}