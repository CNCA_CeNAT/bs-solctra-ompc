include(${CMAKE_SOURCE_DIR}/cmake/SystemLink.cmake)

set(EXECUTABLE_NAME bs_solctra)

add_executable(${EXECUTABLE_NAME} ${CMAKE_CURRENT_SOURCE_DIR}/src/main_multinode.cpp ${CMAKE_CURRENT_SOURCE_DIR}/src/solctra_multinode.cpp ${CMAKE_CURRENT_SOURCE_DIR}/src/utils.cpp)

target_link_system_library(${EXECUTABLE_NAME} PRIVATE OMPC::Runtime)
target_link_libraries(${EXECUTABLE_NAME} PRIVATE bs_solctra::bs_solctra_options
                                                 bs_solctra::bs_solctra_warnings)

target_include_directories(bs_solctra PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/include)